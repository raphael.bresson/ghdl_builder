__GHDL BUILDER__
-----

__Instructions__
-----
1- install dependencies

```bash
source <ghdl_repo_root>/install_prerequisites.sh
```

2- build ghdl

```bash
bash <ghdl_repo_root>/build_ghdl.sh
```

This will take some time. 
This script build ghdl with gcc-backend and install it in $(pwd)/usr. 
Compilation products are stored in $(pwd)/work.

-----

__How to use__
-----

Before use ghdl you have to launch this command to update your path.

```bash
source <ghdl_install_dir>/ghdl_env.sh
```

Adding this command to your ".bashrc" isn't a good idea as it can override your system's gcc.
