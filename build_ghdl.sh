root_dir=$(pwd)
work_dir=$root_dir/work
install_dir=$root_dir/usr

echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@ BUILD GHDL WITH GCC BACKEND @@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

mkdir -p $work_dir $install_dir

cd $work_dir
wget ftp://ftp.lip6.fr/pub/gcc/releases/gcc-9.3.0/gcc-9.3.0.tar.gz
mkdir gcc && tar -zxvf gcc-9.3.0.tar.gz -C gcc --strip-components 1
cd gcc
./contrib/download_prerequisites
cd ..
git clone "https://github.com/ghdl/ghdl.git" ghdl
cd ghdl
git remote rename origin github
mkdir build
cd build
../configure --with-gcc=$work_dir/gcc --prefix=$install_dir
make copy-sources
mkdir gcc-objs
cd gcc-objs
$work_dir/gcc/configure --prefix=$install_dir \
                        --enable-languages=c,c++,vhdl \
			                  --disable-bootstrap \
                        --disable-lto --disable-multilib --disable-libssp --disable-libgomp \
                        --disable-libquadmath
make -j16 && make install
cd ..
make ghdllib
make install

echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@ GCC BACKEND FOR GHDL SUCCESFULLY BUILT @@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

echo "@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@ Creating env file  @@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@"

touch $root_dir/ghdl_env.sh
echo "export PATH=$install_dir/bin:\$PATH" > $root_dir/ghdl_env.sh

